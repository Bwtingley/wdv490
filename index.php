<?php

session_cache_limiter('none');
session_start();

if(empty($_SESSION['validUser'])){
    $_SESSION['validUser'] = "no";
}

//
//NAVBAR LOGIN//OPTIONS
//

if($_SESSION['validUser'] == "no"){
    //
    //LOGIN DROPDOWN
    //
        $navBarOptions = "        
        <li class='dropdown'>
          <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Login<span class='caret'></span></a>
            <form method='post' name='loginForm' action='php/login.php' class='dropdown-menu  bt-login-dropdown'>
    
                <p>Username:</p> 
                <input type='text' class='blackText' name='inUsername' />
                <p>Password:</p>
                <input type='password' class='blackText' name='inPassword' />
                <p><input type='submit' class='blackText' name='login' value='Login' /><input type='reset' class='blackText' name='reset' /></p>
            </form>
        </li>
        "
        ;
}

else{
    $navBarOptions = "

                    <li><a href='php/logout.php'>Logout</a></li>

                    ";
    }

?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Crystal Creek Townhomes Association - Home</title>

        <link rel="author" href="humans.txt">

        <!-- Jquery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

        <!-- Bootstrap Required -->
        <link rel="stylesheet" href="inc/bootstrap/css/bootstrap.min.css">
        <script src="inc/bootstrap/js/bootstrap.min.js"></script>

        <!-- Custom -->
        <!-- CSS -->
		<link rel="stylesheet" href="styles/styles.css">
	</head>
    
    <body>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
	<div class="page-header">
		<h1 class="header-text">Crystal</h1>
        <h1 class="header-text">Creek</h1>
	</div>
		</div>
	</div>
</div>


<div class="container">
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="index.php">Home</a></li>
        <li><a href="#about">Calender</a></li>

        		<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Board Info<span class="caret"></span></a>
          					<ul class="dropdown-menu">
            					<li><a href="Board_members.html">Board Members</a></li>
            					<li><a href="Board_meeting_schedule.html">Board Meeting Schedule</a></li>
          					</ul>
        		</li>

        		<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Public Documents<span class="caret"></span></a>
          					<ul class="dropdown-menu">
            					<li><a href="docs/CCTHA_Rules_and_Regulations_07_2016.pdf">Rules</a></li>
            					<li><a href="docs/CCTHA_Articles_Inc_03_20_2015.pdf">Articles of Incorporation</a></li>
            					<li><a href="docs/CCTHA_Covenants_Conditions_Restrictions_07_2016.pdf">Covenants</a></li>
            					<li><a href="docs/CCTHA_Bylaws_07_2016.pdf">Bylaws</a></li>
          					</ul>
        		</li>
	    <li><a href="Fees_and_assessments.html">Fees & Assessments</a></li>
	    <li><a href="contact.html">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <?php echo $navBarOptions; ?>
      </ul>
    </div>
  </div>
</nav>
</div>

<div class="container">	
	<div class="row bt-body">
		<div class="col-sm-4">
			<div class="row">
    			<div id="myCarousel" class="carousel slide" data-ride="carousel">

<!-- Carousel indicators -->

        			<ol class="carousel-indicators">
            			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            			<li data-target="#myCarousel" data-slide-to="1"></li>
            			<li data-target="#myCarousel" data-slide-to="2"></li>
        			</ol>   

<!-- CAROUSEL PICTURES -->

        			<div class="carousel-inner">
            			<div class="item active">
                			<img src="img/house1.jpg" alt="First Slide">
            			</div>
            			<div class="item">
                			<img src="img/house2.jpg" alt="Second Slide">
            			</div>
            			<div class="item">
                			<img src="img/house3.jpg" alt="Third Slide">
            			</div>
        			</div>

<!-- Carousel controls -->

        			<a class="carousel-control left" href="#myCarousel" data-slide="prev">
            			<span class="glyphicon glyphicon-chevron-left"></span>
        			</a>
        			<a class="carousel-control right" href="#myCarousel" data-slide="next">
            			<span class="glyphicon glyphicon-chevron-right"></span>
        			</a>
    			</div>
                    <div class="row">
                        <h1 class="text-center">Upcoming</h1>
                    </div>
    		</div>
		</div>
		
<!-- MAIN CONTENT -->

		<div class="main col-sm-8">
			<div class="bt-content-background">					

			<h2 class="text-center">About Crystal Creek Townhomes Association</h2>
				<p>
				In the United States, a homeowner association (HOA) is a private association formed by a real estate developer for the purpose of marketing, managing, and selling homes and lots in a residential subdivision. It grants the developer privileged voting rights in governing the association, while allowing the developer to exit financial and legal responsibility of the organization. Typically the developer will transfer ownership of the association to the homeowners after selling a predetermined number of lots. Generally any person who wants to buy a residence within the area of a homeowners association must become a member, and therefore must obey the several restrictions that often limit the owner's choices. Most homeowner associations are incorporated, and are subject to state statutes that govern non-profit corporations and homeowner associations. State oversight of homeowner associations is minimal, and it varies from state to state. Some states, such as Florida[1] and California,[citation needed] have a large body of HOA law. Other states, such as Massachusetts,[citation needed] have virtually no HOA law. Homeowners associations are primarily associated with mid- and late 20th-century and 21st-century residential development.
				</p>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-footer bt-footer">
					<p>Crystal Creek Townhomes Association</p>
					<p>Oakwood Drive and 105th Court</p>
                    <p>Urbandale, Iowa</p>
                    <p>&copy;<span id="copyright">
                        <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>
                            </span> Crystal Creek Townhomes Association</p>
				</div>
			</div>
		</div>
	</div>

</div><!-- END CONTAINER -->




    </body>
</html>