<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Delete Users</title>

<style>
body{
    background-color: #ece9d8;
    text-align:center;
}
table.center {
    margin-left:auto; 
    margin-right:auto;
}
</style>

<?php

	$deleteFormId = $_GET['id'];		//Pull the form example ID from the GET parameter

	require_once('inc/connectDB.php'); //DB connect puts DB connection in $db

?>
</head>

<body>



<?php

echo "<h2>Deleting record number: " . $_GET['id'] . "</h2>";	//Display a message verifying the record to be deleted.  This could be turned into a second confirmation

$sql = "DELETE FROM Users WHERE User_ID = $deleteFormId";
	//echo "<p>The SQL Command: $sql </p>";     //testing
	
if (mysqli_query($db,$sql) )					//process the query
{
	echo "<h1>User has been successfully deleted.</h1>";
	echo "<p><a href='userList.php'>Return to the User List.</a></p>";	
}
else
{
	echo "<h1>You have encountered a problem with your delete.</h1>";
	echo "<h2 style='color:red'>" . mysqli_error($db) . "</h2>";
	echo "<p><a href='userList.php'>Return to the User List.</a></p>";
}

mysqli_close($db);		//close the database connection and free up server resources
?>
</body>
</html>
