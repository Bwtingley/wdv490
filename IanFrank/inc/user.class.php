<?php
require_once('inc/sanitize.inc.php'); //Used on any lines that are inputs not dropdown

class user{
    /* Variable Storage */
    var $userId;
    var $userName;
    var $userPassword;
    var $userSecurityLevel;
    var $userPhone;

    /* Set Variables */
    function setData($dataArray){

        $this->userId = $dataArray['id'];
        $this->userName = sanitize_html_string($dataArray['Name']); //Turn characters into HTML char code
        $this->userPassword = sanitize_html_string($dataArray['Password']);
        $this->userSecurityLevel = sanitize_html_string($dataArray['SecurityLevel']);
        $this->userPhone = sanitize_html_string($dataArray['Phone']);
    }

    
    /* Get information from DB based on ID to be used for edits*/
    function load($Id){
        $success = false;
        
require_once('inc/connectDB.php'); //DB connect puts DB connection in $db

        if ($db){
            
            $loadUserSQL = "SELECT * FROM Users WHERE User_ID = " .  
                mysqli_real_escape_string($db, $Id);            
            //var_dump($loadUserSQL);die;          //Testing
            $rs = mysqli_query($db, $loadUserSQL);
            
            if ($rs){                
                $userData = mysqli_fetch_assoc($rs);
//                var_dump($userData);die;
                $this->setData($userData);
                $success = ($this->userId > 0 ? true : false); //Checks to make sure if it got anything back, if so returns true
            }else{
                echo mysqli_error($db);
                die;                
            }
        }else{
            echo mysqli_error($db);
            die;
        }
        
        return $success;
}


    /* Save Info to DB from updates or inserts*/
    function save(){
        require_once('inc/connectDB.php'); //DB connect puts DB connection in $db
        if ($db){
        
            if ($this->userId > 0) { //Check to see if there is an ID
                // this is an update
                $userUpdateSQL = "UPDATE Users SET " .
                    "User_Name = '" . mysqli_real_escape_string($db, $this->userNamed) . "', " .
                    "User_Password = '" . mysqli_real_escape_string($db, $this->userPassword) . "', " .
                    "User_Security_Level = '" . mysqli_real_escape_string($db, $this->userSecurityLevel) . "', " .
                    "User_Phone = '" . mysqli_real_escape_string($db, $this->userPhone) . "', " .
                    "WHERE User_ID = " . mysqli_real_escape_string($db, $this->userId);


                $rs = mysqli_query($db, $userUpdateSQL);     
                if (!$rs) {
                    echo mysqli_error($db);
echo "<br>";
 var_dump($userUpdateSQL);
                    die;
                }

            }else{
                // this is an insert
                $userInsertSQL = "INSERT INTO Users SET " . 
                    "User_Name = '" . mysqli_real_escape_string($db, $this->userName) . "', " .
                    "User_Password = '" . mysqli_real_escape_string($db, $this->userPassword) . "', " .
                    "User_Security_Level = '" . mysqli_real_escape_string($db, $this->userSecurityLevel) . "', " .
                    "User_Phone = '" . mysqli_real_escape_string($db, $this->userPhone) . "' ";

                $rs = mysqli_query($db, $userInsertSQL);     
                if (!$rs) {
                    echo mysqli_error($db);
echo "<br>";
 var_dump($userInsertSQL);
                    die;
                }else{
                    $this->userId = mysqli_insert_id($db);
                }

            }
        }
    }


function validate(){
        $errorsArray = array();
        
        if (empty($this->userName)){
            $errorsArray['userName'] = "Location is required.";
        }
      
        return $errorsArray;
    }

/*  */
function getUsersList(){ //Input incase a sort would be used
        $rs = null;
        
        require_once('inc/connectDB.php'); //DB connect puts DB connection in $db
        
        if ($db){
            
            $getUserSQL = "SELECT * FROM Users";

            $whereClause = "";
            
            
            $getUserSQL .= (!empty($whereClause) ? " WHERE " . $whereClause : "" );

            
            $rs = mysqli_query($db, $getUserSQL);     
            if (!$rs){
                echo mysqli_error($db);
                die;
            }            
        }
        return $rs;
}    



}
?>