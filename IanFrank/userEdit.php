<style>

body{
    background-color: #ece9d8;
    text-align:center;
}

table.center {
    margin-left:auto; 
    margin-right:auto;
}

</style>

<?php
require_once('inc/user.class.php');

$user = new user();

if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0)
{
    $user->load($_REQUEST['id']);
}

if (isset($_POST['id'])){

    $user->setData($_POST);
    
    
    if (count($errors) == 0){
        $user->save();
        header("Location: userList.php");
        exit;
    }
    
}

?>

<html>
    <body>
        
        <?php 
           if(!empty($errors)){
             var_dump($errors); 
           }
         ?>
            

        <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="POST">
            Name:<br> <input type="text" name="Name" required="required" value="<?php echo htmlspecialchars_decode($user->userName, ENT_HTML5); ?>"/><br> <!-- Turns the sanitized stuff back into HTML -->

            Password:<br> <input type="text" name="Password" required="required" value="<?php echo htmlspecialchars_decode($user->userPassword, ENT_HTML5); ?>"/><br> <!-- Turns the sanitized stuff back into HTML -->

            User Security Level:
<br><select name="SecurityLevel">
<option value="1" <?php if ($user->userSecurityLevel=="1") echo 'selected="selected"';?>>Member</options>
<option value="5" <?php if ($user->userSecurityLevel=="5") echo 'selected="selected"';?>>Board Member</options>
<option value="10" <?php if ($user->userSecurityLevel=="10") echo 'selected="selected"';?>>Board Leader</options>
</select><br>

Phone Number:<br> <input type="text" name="Phone" value="<?php echo htmlspecialchars_decode($user->userPhone, ENT_HTML5); ?>"/><br> <!-- Turns the sanitized stuff back into HTML -->

            <input type="hidden" name="id" value="<?php echo $user->userId; ?>"/><br>
            
            <input type="submit" name="submit">
        </form>

<p><a href='userList.php'>Return to User Form</a></p>   
    
    </body>
</html>