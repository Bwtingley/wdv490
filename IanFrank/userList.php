<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Board Memeber List</title>

<style>
body{
    background-color: #ece9d8;
    text-align:center;
}
table.center {
    margin-left:auto; 
    margin-right:auto;
}
</style>

<?php

require_once('inc/sanitize.inc.php');
require_once('inc/user.class.php');
$user = new user();

$result = $user->getUsersList();
?>

</head>

<body>

<h1>User Form</h1>

<h2><a href='../'>Admin Page</a></h2>

<div>

	<a href='userEdit.php'>Add new User</a>

	<table border="1" class="center">
	<tr>
		<th>Name</th>
		<th>Password</th>
		<th>Security Level</th>
		<th>Phone Number</th>
		<th>Update</th>
		<th>Delete</th>
	</tr>    
<?php
	while($row = mysqli_fetch_array($result)){		//Turn each row of the result into an associative array 
  	
		//For each row you found int the table create an HTML table in the response object
  		echo "<tr>";
  		echo "<td>" . $row['User_Name'] . "</td>";
  		echo "<td>" . $row['User_Password'] . "</td>";
  		echo "<td>" . $row['User_Security_Level'] . "</td>";
  		echo "<td>" . $row['User_Phone'] . "</td>";
		echo "<td><a href='userEdit.php?id=" . $row['User_ID'] . "'>Update</a></td>";
      	echo "<td><a href='deleteForm.php?id=" . $row['User_ID'] ."' onclick='return confirm()'>Delete</a></td>";
  		echo "</tr>";
  	}

	//echo "</table>";		//Placed this command in the HTML instead of using the echo
?>
	</table>

</div>


</body>
</html>